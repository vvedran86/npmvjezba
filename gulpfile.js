const

    // dev mode
    devMode = false,

    // modules
    gulp = require('gulp'),
    newer = require('gulp-newer'),
    cleanHtml = require('gulp-htmlclean'),
    noop = require('gulp-noop'),
    sync = require('browser-sync').create(),

    src = 'src/',
    build = 'www';

    // tasks
    function html() {
        return gulp.src(src + 'html/**/*')
                   .pipe(newer(build))
                   .pipe(devMode ? noop() : cleanHtml())
                   .pipe(gulp.dest(build))
                   .pipe(sync.stream());
    }

    function watch(done) {
        // Sync server
        sync.init({
            server: {
                baseDir: './www',
                startPath: 'index.html'
            }
        });
        
        // HTML changes
        gulp.watch(src + 'html/**/*', html);
        done();
    }

// run rasks
exports.html = html;
exports.watch = watch;

exports.build = gulp.parallel(exports.html);
exports.default = gulp.series(exports.build, exports.watch);